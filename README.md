# NL_Prop
This is a Matlab GUI (app) designed to control a PXI 1073 system (using PXI 4461 and PXIe 4464 cards).

It is setup to produce short sine bursts, or steady sine tones, send to a loudspeaker, with 4 microphone signals recorded in response.
